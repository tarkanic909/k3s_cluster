#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
apt -y install git jq

# hostname
hostnamectl set-hostname master --static

# Create User
useradd -s /bin/bash -c "Admin" -m jozef
echo "Passw0rd" | passwd --stdin jozef

# Set sudo
echo "jozef ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/jozef/.ssh
cat <<EOF | tee -a /home/michal/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDj+mqtKu7Hr6q8XCOf+HKwejqw2CgHdKjJVZsP4bcegzH32z3OMBwrXHlhgyQhDQY2Q4YFFWD8fcaMuIHGNbLnVwLzvgpb47t+nDtzH9kT0G7uPQvtIj+/6oHuLxfrQdacOTEA8YXCWvrcLUQ5TSKeGq9VlQGKx5T+aDfxHb+ATUN/YDzwet3nqfIqEp4Kx+KDoHKnQ7Ek4dSns8940wef6jMtnBDLwsXQYy1BKBFFYXVtIBEZXNmq7Jl6K7KR+3LP1j1uNBeyRAU3DHD8VVpaKQEDpBDxlZgFcm+Fhg6xA3BPPr5F2KtT49zm+5zX1MS+hVwS8SIMsXuVx9Eytyp jozef
EOF
# Set proper permissions
chown -R jozef /home/jozef/.ssh
chmod 700 /home/michal/.ssh
chmod 600 /home/michal/.ssh/authorized_keys
##

## nginx
apt update
apt -y install nginx
sed -i 's/listen 80/listen 8080/g' /etc/nginx/sites-available/default
systemctl restart nginx 
#

## Init K3S
curl -sfL https://get.k3s.io | sh -
# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
cp /var/lib/rancher/k3s/server/token /var/www/html/token
chmod 644 /var/www/html/token
#

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
EOF
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                 Welcome to OSC Environment!               ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││            This is your K3S multi-node cluster.           ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
##

