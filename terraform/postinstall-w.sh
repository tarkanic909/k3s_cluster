#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`
##

# Install packages
apt -y install git
##

# Create User
useradd -s /bin/bash -c "Admin" -m jozef
echo "Passw0rd" | passwd --stdin jozef
echo "jozef ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
##

# Deploy SSH keys
mkdir /home/jozef/.ssh
cat <<EOF | tee -a /home/michal/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDj+mqtKu7Hr6q8XCOf+HKwejqw2CgHdKjJVZsP4bcegzH32z3OMBwrXHlhgyQhDQY2Q4YFFWD8fcaMuIHGNbLnVwLzvgpb47t+nDtzH9kT0G7uPQvtIj+/6oHuLxfrQdacOTEA8YXCWvrcLUQ5TSKeGq9VlQGKx5T+aDfxHb+ATUN/YDzwet3nqfIqEp4Kx+KDoHKnQ7Ek4dSns8940wef6jMtnBDLwsXQYy1BKBFFYXVtIBEZXNmq7Jl6K7KR+3LP1j1uNBeyRAU3DHD8VVpaKQEDpBDxlZgFcm+Fhg6xA3BPPr5F2KtT49zm+5zX1MS+hVwS8SIMsXuVx9Eytyp jozef
EOF
# Set proper permissions
chown -R michal /home/jozef/.ssh
chmod 700 /home/jozef/.ssh
chmod 600 /home/jozef/.ssh/authorized_keys
##

## Init K3S
curl -sfL https://get.k3s.io | sh -
systemctl stop k3s
##

## Join to the Cluster
sleep 3m
masterip=10.0.1.69
wget http://${masterip}:8080/token 
mastertoken=`cat token`
k3s agent --server https://${masterip}:6443 --token ${mastertoken}
##

